<?php

namespace Tests\Feature\Admin;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test index.
     *
     * @return void
     */
    public function testIndex()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.users.index');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.users.index');
        $response->assertViewHas('users');
    }

    /**
     * A basic feature test create.
     *
     * @return void
     */
    public function testCreate()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.users.create');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.users.create');
        $response->assertViewHas('roles');
    }

    /**
     * A basic feature test store.
     *
     * @return void
     */
    public function testStore()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.users.store');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $user = factory(User::class)->make();
        $user->password_confirmation = $user->password;

        $response = $this->actingAs($authUser)
            ->post($url, $user->makeVisible(['password'])->toArray());

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.users.index'));

        $this->assertDatabaseHas('users', $user->only([
            'name',
            'email',
            'role_id'
        ]));
    }

    /**
     * A basic feature test edit.
     *
     * @return void
     */
    public function testEdit()
    {
        $authUser = factory(User::class)->create();
        $user = factory(User::class)->create();
        $url = route('admin.users.edit', $user);

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.users.edit');
        $response->assertViewHas('user', $user);
    }

    /**
     * A basic feature test update.
     *
     * @return void
     */
    public function testUpdate()
    {
        $authUser = factory(User::class)->create();
        $user = factory(User::class)->create();
        $url = route('admin.users.update', $user);

        $response = $this->put($url);
        $response->assertRedirect(route('login'));

        $userModified = factory(User::class)->make();

        $response = $this->actingAs($authUser)
            ->put($url, $userModified->toArray());

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.users.index'));

        $this->assertDatabaseHas('users', $userModified->toArray());
        $this->assertDatabaseMissing('users', $user->toArray());
    }

    /**
     * A basic feature test destroy.
     *
     * @return void
     */
    public function testDestroy()
    {
        $authUser = factory(User::class)->create();
        $user = factory(User::class)->create();
        $url = route('admin.users.destroy', $user);

        $response = $this->delete($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->delete($url);

        $response->assertRedirect(route('admin.users.index'));
        $this->assertSoftDeleted('users', $user->toArray());
    }

    /**
     * A basic feature test restore.
     *
     * @return void
     */
    public function testRestore()
    {
        $authUser = factory(User::class)->create();
        $user = factory(User::class)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);
        $url = route('admin.users.restore', $user);

        $response = $this->patch($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->patch($url);

        $response->assertRedirect(route('admin.users.index', ['only_trashed' => 1]));
        $this->assertDatabaseHas(
            'users',
            array_merge($user->toArray(), ['deleted_at' => null])
        );
    }
}
