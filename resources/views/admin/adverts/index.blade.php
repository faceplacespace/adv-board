@extends('layouts.admin.app')

@section('content')

        <div class="container">
            <div class="row">
                <nav class="navbar navbar-light">
                    <a href="{{ route('admin.adverts.create') }}" class="btn btn-success">Create advert</a>
                </nav>
                @include('admin.filters')
                <table class="table mt-4">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($adverts as $advert)
                    <tr>
                        <th scope="row">{{ $advert->id }}</th>
                        <td>{{ $advert->title }}</td>
                        <td>{{ $advert->description }}</td>
                        <td>
                            @if($advert->trashed())
                                <input type="submit" class="btn btn-dark" form="restore-item"
                                       formaction="{{ route('admin.adverts.restore', $advert) }}" value="Restore">
                            @else
                                <a href="{{ route('admin.adverts.edit', $advert) }}" class="btn btn-warning">
                                    Edit
                                </a>
                                <input type="submit" class="btn btn-danger" form="delete-item"
                                       formaction="{{ route('admin.adverts.destroy', $advert) }}" value="Delete">
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <form id="restore-item" method="POST">
                    @method('PATCH')
                    @csrf
                </form>
                <form id="delete-item" method="POST">
                    @method('DELETE')
                    @csrf
                </form>
                {{ $adverts->links() }}
            </div>
        </div>

@endsection
