<?php

use App\Advert;
use App\Category;
use App\Image;
use App\User;
use Illuminate\Database\Seeder;
use function foo\func;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Advert::chunk(200, function ($adverts) {
            $adverts->each(function ($advert) {
                factory(Image::class, rand(0, 8))->make([
                    'imageable_id' => $advert->id,
                    'imageable_type' => Advert::class
                ])->each(function ($image) {
                    $image->save();
                });
            });
        });

        Category::chunk(200, function ($categories) {
            $categories->each(function ($category) {
                factory(Image::class)->make([
                    'imageable_id' => $category->id,
                    'imageable_type' => Category::class
                ])->save();
            });
        });

        User::chunk(200, function ($users) {
            $users->each(function ($user) {
                factory(Image::class)->make([
                    'imageable_id' => $user->id,
                    'imageable_type' => User::class
                ])->save();
            });
        });
    }
}
