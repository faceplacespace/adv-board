<?php

namespace Tests\Feature\Admin;

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test index.
     *
     * @return void
     */
    public function testIndex()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.categories.index');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.categories.index');
        $response->assertViewHas('categories');
    }

    /**
     * A basic feature test create.
     *
     * @return void
     */
    public function testCreate()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.categories.create');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.categories.create');
    }

    /**
     * A basic feature test store.
     *
     * @return void
     */
    public function testStore()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.categories.store');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $category = factory(Category::class)->make();

        $response = $this->actingAs($authUser)
            ->post($url, $category->toArray());

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.categories.index'));

        $this->assertDatabaseHas('categories', $category->toArray());
    }

    /**
     * A basic feature test edit.
     *
     * @return void
     */
    public function testEdit()
    {
        $authUser = factory(User::class)->create();
        $category = factory(Category::class)->create();
        $url = route('admin.categories.edit', $category);

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.categories.edit');
        $response->assertViewHas('category', $category);
    }

    /**
     * A basic feature test update.
     *
     * @return void
     */
    public function testUpdate()
    {
        $authUser = factory(User::class)->create();
        $category = factory(Category::class)->create();
        $url = route('admin.categories.update', $category);

        $response = $this->put($url);
        $response->assertRedirect(route('login'));

        $categoryModified = factory(Category::class)->make();

        $response = $this->actingAs($authUser)
            ->put($url, $categoryModified->toArray());

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.categories.index'));

        $this->assertDatabaseHas('categories', $categoryModified->toArray());
        $this->assertDatabaseMissing('categories', $category->toArray());
    }

    /**
     * A basic feature test destroy.
     *
     * @return void
     */
    public function testDestroy()
    {
        $authUser = factory(User::class)->create();
        $category = factory(Category::class)->create();
        $url = route('admin.categories.destroy', $category);

        $response = $this->delete($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->delete($url);

        $response->assertRedirect(route('admin.categories.index'));
        $this->assertSoftDeleted('categories', $category->toArray());
    }

    /**
     * A basic feature test restore.
     *
     * @return void
     */
    public function testRestore()
    {
        $authUser = factory(User::class)->create();
        $category = factory(Category::class)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);
        $url = route('admin.categories.restore', $category);

        $response = $this->patch($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->patch($url);

        $response->assertRedirect(route('admin.categories.index', ['only_trashed' => 1]));
        $this->assertDatabaseHas(
            'categories',
            array_merge($category->toArray(), ['deleted_at' => null])
        );
    }
}
