@extends('layouts.admin.app')

@section('content')

    <div class="container">
        <div class="row">
            <nav class="navbar navbar-light">
                <a href="{{ route('admin.users.create') }}" class="btn btn-success">Create user</a>
            </nav>
            @include('admin.filters')
            <div class="container">
                <a class="btn btn-light @if (request()->has('administrator')) active @endif"
                   href="{{ route(Route::currentRouteName(), ['administrator' => 1]) }}">
                    Administrator
                </a>
            </div>
            <table class="table mt-4">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role_name }}</td>
                        <td>
                            @if($user->trashed())
                                <input type="submit" class="btn btn-dark" form="restore-item"
                                       formaction="{{ route('admin.users.restore', $user) }}" value="Restore">
                            @else
                                <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-warning">
                                    Edit
                                </a>
                                <input type="submit" class="btn btn-danger" form="delete-item"
                                       formaction="{{ route('admin.users.destroy', $user) }}" value="Delete">
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <form id="restore-item" method="POST">
                @method('PATCH')
                @csrf
            </form>
            <form id="delete-item" method="POST">
                @method('DELETE')
                @csrf
            </form>
            {{ $users->links() }}
        </div>
    </div>

@endsection
