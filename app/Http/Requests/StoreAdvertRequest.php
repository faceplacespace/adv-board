<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdvertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'min:5', 'max:255'],
            'slug' => ['required', 'unique:adverts,slug', 'alpha_dash', 'max:255'],
            'categories' => ['required', 'array', 'min:1'],
            'description' => ['required', 'min:30', 'max:16000'],
            'image_ids' => ['nullable', 'array'],
            'image_ids.*' => ['integer']
        ];
    }
}
