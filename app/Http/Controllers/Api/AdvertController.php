<?php

namespace App\Http\Controllers\API;

use App\Advert;
use App\Http\Controllers\Controller;
use App\Http\Resources\AdvertResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $perPage = request()->query('per_page') ?? null;

        $advertsQuery = Advert::query();

        $advertsQuery->when(request('category_id'), function ($query, $categoryId) {
            return $query->whereHas('categories', function ($query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            });
        })->when(request('similar_by_id'), function ($query, $similarById) {
            return $query->whereHas('categories', function ($query) use ($similarById) {
                $query->whereIn('category_id', Advert::find($similarById)->categories->pluck('id'));
            })->where('id', '!=', $similarById)
                ->inRandomOrder();
        });

        $adverts = $advertsQuery->with('images')->latest('updated_at')->paginate($perPage);

        return AdvertResource::collection($adverts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
