<?php

namespace App\Console\Commands;

use App\Advert;
use App\Mail\NewAdverts;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendNewAdverts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:new-adverts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect ads and send email to a random user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Sending email...');

        $user = User::inRandomOrder()->first();
        $adverts = Advert::where('created_at', '>', now()->subDay()->toDateTimeString())
            ->get();

        Mail::to($user)->send(new NewAdverts($adverts));

        $this->info("Email has been sent.");
    }
}
