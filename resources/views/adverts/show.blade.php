@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="my-4">{{ $advert->title }}</h1>
        <span class="badge badge-pill badge-secondary mb-2 p-2">Views: {{ $advert->views }}</span>
        <div class="row">
            <div class="col-md-4">
                <p>{{ $advert->description }}</p>
            </div>
        </div>
        <div class="files row text-center text-lg-left">
            @foreach($advert->images as $image)
                <div class="col-lg-3 col-md-4 col-6">
                    <image-list-item-component
                        src="{{ asset("storage/images/{$image->thumb_path}") }}"
                        alt="{{ $image->name }}"
                    ></image-list-item-component>
                </div>
            @endforeach
        </div>
        <advert-listing-component
            url="{{ route('api.adverts.index') }}"
            :per-page=3
            :similar-by-id={{ $advert->id }}
            :show-more-button=false
            title="Similar adverts">
        </advert-listing-component>
    </div>
@endsection
