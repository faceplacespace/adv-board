<div class="container mt-3">
    <a class="btn btn-light {{ request()->hasAny(['only_trashed', 'administrator']) ? '' : 'active' }}"
       href="{{ route(Route::currentRouteName()) }}">
        All
    </a>
    <a class="btn btn-light @if (request()->has('only_trashed')) active @endif"
       href="{{ route(Route::currentRouteName(), ['only_trashed' => 1]) }}">
        Only deleted
    </a>
</div>
