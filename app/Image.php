<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App
 */
class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'files',
        'imageable_id',
        'imageable_type'
    ];

    /**
     * Get the owning imageable model.
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        return "{$this->created_at->format('Y/m')}/{$this->name}";
    }

    /**
     * @return string
     */
    public function getThumbPathAttribute()
    {
        list('filename' => $filename, 'extension' => $extension) = pathinfo($this->name);
        return "{$this->created_at->format('Y/m')}/{$filename}_thumbnail.{$extension}";
    }

}
