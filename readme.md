# Advertising Site on Laravel and Vue.js

## Demo

* Host http://faceplnp.beget.tech 

## Installation and deployment

1. `git clone https://gitlab.com/faceplacespace/adv-board.git my_project_name`
2. `cd my_project_name`
3. `composer install`
4. `touch database/database.sqlite`
5. `cp .env.example .env`
6. `php artisan key:generate`
7. Fill the .env file
8. `php artisan storage:link`
8. `php artisan migrate --seed`
9. `php artisan serve`
10. Sign up