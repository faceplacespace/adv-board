<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreImageRequest;
use App\Image;
use App\Jobs\CreateImageThumbnails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreImageRequest $request
     * @return JsonResponse
     */
    public function store(StoreImageRequest $request)
    {
        $data = $request->validated();
        $now = now();

        foreach ($data['files'] as $file) {
            $imagePath = "{$now->format('Y/m')}";
            Storage::makeDirectory("public/images/{$imagePath}");

            $pathInfo = pathinfo($file->store("public/images/{$imagePath}"));

            $image = Image::create([
                'name' => $pathInfo['basename'],
                'imageable_id' => $data['imageable_id'],
                'imageable_type' => $data['imageable_type'],
            ]);

            dispatch(new CreateImageThumbnails($image));
        }

        return response()->json([
            'url' => asset("storage/images/{$image->image_path}"),
            'name' => $image->name,
            'id' => $image->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param App\Image $image
     * @return JsonResponse
     */
    public function destroy(Image $image)
    {
        if (Storage::delete([
            "public/images/{$image->image_path}",
            "public/images/{$image->thumb_path}"
        ])) {
            $image->delete();
        }

        return response()->json(['result' => true]);
    }
}
