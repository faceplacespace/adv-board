@extends('layouts.app')

@section('content')
    <div class="container">
            <h1 class="mb-3">Categories</h1>
        <div class="row justify-content-center">
            @foreach ($categories as $category)
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <div class="card-body">
                            <p class="card-text">{{ $category->name }}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ route('categories.show', $category) }}"
                                       class="btn btn-sm btn-outline-secondary">View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="d-flex justify-content-center w-100 mt-3">
                {{ $categories->links() }}
            </div>
        </div>
    </div>
@endsection
