<?php

/** @var Factory $factory */

use App\Image;
use App\Model;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

$factory->define(Image::class, function (Faker $faker) {
    $randomDate = $faker->dateTimeBetween('-1 year');
    return [
        'name' => function ($image) use ($faker, $randomDate) {
            $imagePath = "{$randomDate->format('Y/m')}/";
            Storage::makeDirectory("public/images/{$imagePath}");

            $image = $faker->image(storage_path("app/public/images/{$imagePath}"), 400, 400, 'cats', false);

            list('filename' => $filename, 'extension' => $extension) = pathinfo($image);
            $thumbName = "{$filename}_thumbnail.{$extension}";

            $thumb = InterventionImage::make(public_path("storage/images/{$imagePath}/{$image}"));
            $thumb->resize(100,100)->save(storage_path("app/public/images/{$imagePath}/{$thumbName}"));

            return $image;
        },
        'imageable_id' => '',
        'imageable_type' => '',
        'created_at' => $randomDate->format('Y-m-d H:i:s'),
        'updated_at' => $randomDate->format('Y-m-d H:i:s')
    ];
});
