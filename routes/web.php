<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController');
Route::resource('adverts', 'AdvertController')->only(['index', 'show']);
Route::resource('categories', 'CategoryController')->only(['index', 'show']);
Route::resource('images', 'ImageController')->only(['store', 'destroy']);

Auth::routes();

Route::namespace('Admin')
    ->prefix('admin')
    ->name('admin.')
    ->middleware('auth')
    ->group(function () {
        Route::get('/', 'IndexController')->name('index');
        Route::patch('adverts/{advert}/restore', 'AdvertController@restore')
            ->name('adverts.restore');
        Route::resource('adverts', 'AdvertController')
            ->except(['show']);
        Route::patch('categories/{category}/restore', 'CategoryController@restore')
            ->name('categories.restore');
        Route::resource('categories', 'CategoryController')
            ->except(['show']);
        Route::patch('users/{user}/restore', 'UserController@restore')
            ->name('users.restore');
        Route::resource('users', 'UserController')
            ->except(['show']);
        Route::resource('profile', 'ProfileController')
            ->only(['show', 'edit', 'update'])
            ->parameters(['profile' => 'user']);
    });
