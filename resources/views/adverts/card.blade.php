<div class="col-md-4">
    <div class="card mb-4 shadow-sm">
        @if($advert->images->isNotEmpty())
            <img class="card-img-top"
                 src="{{ asset("storage/images/{$advert->images->first()->image_path}") }}">
        @endif
        <div class="card-body">
            <p class="card-text">{{ $advert->title }}</p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="{{ route('adverts.show', $advert) }}"
                       class="btn btn-sm btn-outline-secondary">View</a>
                </div>
            </div>
        </div>
    </div>
</div>
