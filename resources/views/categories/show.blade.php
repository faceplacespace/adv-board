@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="my-4">{{ $category->name }}</h1>
        <div class="row">
            <div class="col-md-10 mb-3">
                <p>{{ $category->description }}</p>
            </div>
        </div>
        <advert-listing-component
            url="{{ route('api.adverts.index') }}"
            :per-page=9
            :page=1
            :category-id="{{ $category->id }}">
        </advert-listing-component>
    </div>
@endsection
