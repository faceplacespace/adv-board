<?php

/** @var Factory $factory */

use App\Advert;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Advert::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence,
        'slug' => $faker->slug,
        'description' => $faker->paragraph,
        'user_id' => factory(User::class),
        'views' => 0
    ];
});
