@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <h2 class="mt-3">Create advert</h2>
            </div>
            <form class="advert-create col-lg-6 offset-lg-3" method="post" action="{{ route('admin.adverts.store') }}">
                @csrf
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}">
                    @error('title')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" class="form-control @error('slug') is-invalid @enderror" id="slug" name="slug" value="{{ old('slug') }}">
                    @error('slug')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="categories">Category:</label>
                    <select class="form-control @error('categories') is-invalid @enderror" multiple id="categories" name="categories[]">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('categories')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="form-check-label" for="description">Description:</label>
                    <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="5">{{ old('description') }}</textarea>
                    @error('description')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{ route('admin.adverts.index') }}" class="btn btn-light pull-right">Chancel</a>
            </form>
        </div>
        <div class="container">
            <form method="post" action="{{ route('images.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="imageable_id" value="">
                <input type="hidden" name="imageable_type" value="">
                <div class="col-6 float-right">
                    <div class="custom-file">
                        <input class="custom-file-input" id="fileupload" type="file" name="files[]"
                               data-url="{{ route('images.store') }}" multiple>
                        <label class="custom-file-label" for="validatedCustomFile">Add image...</label>
                    </div>
                </div>
            </form>
            <br>
            <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Images</h1>

            <hr class="mt-2 mb-5">

            <div class="files row text-center text-lg-left">

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(() => {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            });

            $('#fileupload').fileupload({
                dataType: 'json',
                add: (e, data) => {
                    $('input.custom-file-input').removeClass('is-invalid');

                    let uploadErrors = [];
                    let acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;

                    $.each(data.originalFiles, (index, file) => {
                        if (file['type'].length && !acceptFileTypes.test(file['type'])) {
                            uploadErrors.push('Not an accepted file type');
                            return false;
                        }

                        if (file['size'].length && file['size'] > 5000000) {
                            uploadErrors.push('Filesize is too big');
                            return false;
                        }
                    });

                    if (uploadErrors.length > 0) {
                        $('input.custom-file-input').addClass('is-invalid');
                        $('label.custom-file-label').after(`<span class="invalid-feedback">${uploadErrors.join("\n")}</span>`);
                        return false;
                    } else {
                        data.submit();
                    }
                },
                done: (e, data) => {
                    let url = data.jqXHR.responseJSON.url;
                    let name = data.jqXHR.responseJSON.name;
                    let id = data.jqXHR.responseJSON.id;

                    $('div.files').append(`<div class="col-lg-3 col-md-4 col-6">` +
                        `<div class="d-block mb-4 h-100">` +
                        `<img class="img-fluid img-thumbnail" src="${url}" alt="">` +
                        `<a class="delete-link" data-id="${id}" href="/images/${name}">delete</a>` +
                        `</div></div>`);

                    $('form.advert-create').append(`<input type="hidden" name="image_ids[]" value="${id}">`);

                    $('div.files').before('<div class="alert alert-success" role="alert">Images successfully added.</div>');
                    setTimeout(() => {
                        $('div.alert-success').fadeOut();
                    }, 2000);
                },
                fail: (e, data) => {
                    let errors = data.jqXHR.responseJSON;
                    $('input.custom-file-input').addClass('is-invalid');
                    $('label.custom-file-label').after(`<span class="invalid-feedback">${errors.message}</span>`);
                }
            });

            $('div.files').on('click', 'a.delete-link', (e) => {
                e.preventDefault();

                $el = $(e.currentTarget);

                $.ajax({
                    url: $el.attr('href'),
                    type: 'POST',
                    data: {_method: 'DELETE'},
                    success: (result) => {
                        $(`input[value="${$el.data('id')}"]`).remove();
                        $el.parent().fadeOut(200, () => {
                            $el.parent().parent().remove();
                        });
                    }
                });
            })
        });
    </script>
@endsection
