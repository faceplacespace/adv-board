@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-light">
                    <h1 class="my-4">{{ $user->name }}</h1>
                    <a href="{{ route('admin.profile.edit', $user) }}" class="btn btn-success">Edit profile</a>
                </nav>
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row">Name</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Email</th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
