<?php

namespace App\Console\Commands;

use App\Advert;
use App\Category;
use Illuminate\Console\Command;

class DeleteOldModels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:models';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Force deleting models deleted more then a week ago.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Force deleting models...');

        $count = 0;
        $prevWeek = now()->subWeek()->toDateTimeString();

        Advert::onlyTrashed()->where('deleted_at', '<', $prevWeek)
            ->each(function ($advert) use (&$count) {
                $advert->forceDelete();
                $count++;
            });

        Category::onlyTrashed()->where('deleted_at', '<', $prevWeek)
            ->each(function ($category) use (&$count) {
                $category->forceDelete();
                $count++;
            });

        $this->info("{$count} models was force deleted.");
    }
}
