<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const ID_ROLE_ADMINISTRATOR = 1;
    const ID_ROLE_CLIENT = 2;

    const NAME_ROLE_ADMINISTRATOR = 'Administrator';
    const NAME_ROLE_CLIENT = 'Client';

    const ROLES = [
        self::ID_ROLE_ADMINISTRATOR => self::NAME_ROLE_ADMINISTRATOR,
        self::ID_ROLE_CLIENT => self::NAME_ROLE_CLIENT
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the user's role name.
     *
     * @param  User $user
     * @return string
     */
    public function getRoleNameAttribute()
    {
        return self::ROLES[$this->role_id];
    }

    /**
     * Get the user's image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * The adverts that belong to the user.
     */
    public function adverts()
    {
        return $this->hasMany(Advert::class);
    }

    /**
     * Scope a query to only include users with administrator role
     *
     * @param Builder $query
     * @param string $slug
     * @return Builder
     */
    public function scopeAdministrator($query)
    {
        return $query->where('role_id', User::ID_ROLE_ADMINISTRATOR);
    }

    /**
     * Scope a query to only include users with client role
     *
     * @param Builder $query
     * @param string $slug
     * @return Builder
     */
    public function scopeClient($query)
    {
        return $query->where('role_id', User::ID_ROLE_CLIENT);
    }
}
