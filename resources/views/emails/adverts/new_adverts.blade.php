<h1>New adverts</h1>
<ol>
@foreach($adverts as $advert)
    <li><a href="{{ route('adverts.show', $advert) }}">{{ $advert->title }}</a></li>
@endforeach
</ol>
