<?php

namespace Tests\Feature\Api;

use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test index.
     *
     * @return void
     */
    public function testIndex()
    {
        $perPage = 6;

        factory(Category::class, 10)->create();

        $response = $this->getJson(route('api.categories.index', [
                'per_page' => $perPage,
                'random_order' => 1
            ]));

        $response
            ->assertStatus(200)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJsonCount($perPage, 'data');
    }
}
