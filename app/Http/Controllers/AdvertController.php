<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert;
use Illuminate\Http\Response;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $adverts = Advert::with('images')
            ->latest('updated_at')
            ->paginate();

        return view('adverts.index', ['adverts' => $adverts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param App\Advert $advert
     * @return Response
     */
    public function show(Advert $advert)
    {
        $similarAdverts = Advert::with('images')
            ->whereHas('categories', function ($query) use ($advert) {
                $query->whereIn('category_id', $advert->categories->pluck('id'));
            })
            ->where('id', '!=', $advert->id)
            ->inRandomOrder()
            ->limit(3)
            ->get();

        $advert->increment('views');

        return view('adverts.show', [
            'advert' => $advert,
            'similarAdverts' => $similarAdverts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
