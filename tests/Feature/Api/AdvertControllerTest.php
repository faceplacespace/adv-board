<?php

namespace Tests\Feature\Api;

use App\Advert;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdvertControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * A basic feature test index.
     *
     * @return void
     */
    public function testIndex()
    {
        $perPage = $this->faker->randomNumber(2);
        $totalAdverts = $this->faker->randomNumber(2);
        $lastPage = ceil($totalAdverts / $perPage);
        $lastPageItemsNum = ($totalAdverts % $perPage) ?: $perPage;

        factory(Advert::class, $totalAdverts)->create();

        $response = $this->getJson(route('api.adverts.index', [
            'per_page' => $perPage,
            'page' => $lastPage
        ]));

        $response
            ->assertStatus(200)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJsonCount($lastPageItemsNum, 'data');
    }
}
