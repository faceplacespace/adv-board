<?php

namespace App\Http\Controllers\Admin;

use App\Advert;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAdvertRequest;
use App\Http\Requests\UpdateAdvertRequest;
use App\Image;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

/**
 * Class AdvertController
 * @package App\Http\Controllers\Admin
 */
class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $advertsQuery = Advert::query();

        $advertsQuery->when(request('only_trashed'), function ($query) {
            return $query->onlyTrashed();
        });

        $adverts = $advertsQuery->latest('updated_at')->paginate();

        return view('admin.adverts.index', ['adverts' => $adverts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.adverts.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAdvertRequest $request
     * @return Response
     */
    public function store(StoreAdvertRequest $request)
    {
        $data = $request->validated();

        if (empty($data['slug'])) {
            $data['slug'] = Str::slug($data['title']);
        }

        $data['user_id'] = auth()->user()->id;

        $advert = Advert::create($data);
        $advert->categories()->sync($data['categories']);

        if (isset($data['image_ids'])) {
            foreach ($data['image_ids'] as $imageId) {
                $image = Image::find($imageId);
                $image->update([
                    'imageable_id' => $advert->id,
                    'imageable_type' => Advert::class,
                ]);
            }
        }

        return redirect()->route('admin.adverts.index')
            ->with('status', 'Advert successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Advert $advert
     * @return Response
     */
    public function edit(Advert $advert)
    {
        $categories = Category::all();

        return view('admin.adverts.edit', ['advert' => $advert, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAdvertRequest $request
     * @param Advert $advert
     * @return Response
     */
    public function update(UpdateAdvertRequest $request, Advert $advert)
    {
        $data = $request->validated();

        if (empty($data['slug'])) {
            $data['slug'] = Str::slug($data['title']);
        }

        $advert->update($data);

        $advert->categories()->sync($data['categories']);

        return redirect()->route('admin.adverts.index')
            ->with('status', 'Advert successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Advert $advert
     * @return Response
     */
    public function destroy(Advert $advert)
    {
        $advert->delete();

        return redirect()->route('admin.adverts.index')
            ->with('status', 'Advert successfully deleted!');
    }

    /**
     * Restore the specified resource
     *
     * @param string $slug
     * @return Response
     */
    public function restore($slug)
    {
        Advert::onlyTrashed()->bySlug($slug)->restore();

        return redirect()->route('admin.adverts.index', ['only_trashed' => 1])
            ->with('status', 'Advert successfully restored!');
    }
}
