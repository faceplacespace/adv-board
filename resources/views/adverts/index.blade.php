@extends('layouts.app')

@section('content')
    <div class="container">
        <advert-listing-component
            url="{{ route('api.adverts.index') }}"
            :per-page=9
            :page=1>
        </advert-listing-component>
    </div>
@endsection
