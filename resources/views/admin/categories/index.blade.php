@extends('layouts.admin.app')

@section('content')

        <div class="container">
            <div class="row">
                <nav class="navbar navbar-light">
                    <a href="{{ route('admin.categories.create') }}" class="btn btn-success">Create category</a>
                </nav>
                @include('admin.filters')
                <table class="table mt-4">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <th scope="row">{{ $category->id }}</th>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->description }}</td>
                        <td>
                            @if($category->trashed())
                                <input type="submit" class="btn btn-dark" form="restore-item"
                                       formaction="{{ route('admin.categories.restore', $category) }}" value="Restore">
                            @else
                                <a href="{{ route('admin.categories.edit', $category) }}" class="btn btn-warning">
                                    Edit
                                </a>
                                <input type="submit" class="btn btn-danger" form="delete-item"
                                       formaction="{{ route('admin.categories.destroy', $category) }}" value="Delete">
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <form id="restore-item" method="POST">
                    @method('PATCH')
                    @csrf
                </form>
                <form id="delete-item" method="POST">
                    @method('DELETE')
                    @csrf
                </form>
                {{ $categories->links() }}
            </div>
        </div>

@endsection
