<?php

namespace App\Console\Commands;

use App\Advert;
use App\Category;
use App\Image;
use App\User;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteUnattachedImages
 * @package App\Console\Commands
 */
class DeleteUnattachedImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete unattached images older than 1 day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Deleting unattached images...');

        $count = 0;
        $prevDay = now()->subDay()->toDateTimeString();

        Image::where([
            ['imageable_id', '=', null],
            ['imageable_type', '=', null],
            ['created_at', '<', $prevDay]
        ])->each(function ($image) use (&$count) {
            $this->deleteImage($image);
            $count++;
        });

        Image::whereDoesntHaveMorph(
            'imageable',
            [Advert::class, Category::class, User::class]
        )->where('created_at', '<', $prevDay)->each(function ($image) use (&$count) {
            $this->deleteImage($image);
            $count++;
        });

        $this->info("{$count} unattached images was deleted.");
    }

    /**
     * @param Image $image
     * @throws Exception
     */
    private function deleteImage(Image $image)
    {
        Storage::delete([
            "public/images/{$image->image_path}",
            "public/images/{$image->thumb_path}"
        ]);
        $image->delete();
    }
}
