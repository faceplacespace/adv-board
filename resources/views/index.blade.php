@extends('layouts.app')

@section('content')
    <div class="container">
        <category-listing-component
            url="{{ route('api.categories.index') }}"
            :per-page=6
            :random-order=1>
        </category-listing-component>
        <advert-listing-component
            url="{{ route('api.adverts.index') }}"
            :per-page=9
            :page=1>
        </advert-listing-component>
    </div>
@endsection
