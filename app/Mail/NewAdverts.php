<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewAdverts extends Mailable
{
    use Queueable, SerializesModels;

    private $adverts;
    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Collection $adverts)
    {
        $this->adverts = $adverts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.adverts.new_adverts')
            ->subject('New adverts')
            ->with(['adverts' => $this->adverts]);
    }
}
