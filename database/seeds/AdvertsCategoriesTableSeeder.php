<?php

use App\Advert;
use App\Category;
use Illuminate\Database\Seeder;

class AdvertsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Advert::chunk(200, function ($adverts) {
            foreach ($adverts as $advert) {
                $categories = Category::inRandomOrder()->limit(rand(1, 3))->get();
                $advert->categories()->sync(
                    $categories->pluck('id')
                );
            }
        });
    }
}
