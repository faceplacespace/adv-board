<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        return view('index', [
            'adverts' => Advert::with('images')->inRandomOrder()->limit(9)->get(),
            'categories' => Category::inRandomOrder()->limit(6)->get()
        ]);
    }
}
