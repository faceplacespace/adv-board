<?php

use App\Advert;
use App\User;
use Illuminate\Database\Seeder;

class AdvertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Advert::class, 40)->make([
            'user_id' => null
        ])->each(function ($advert) {
            $advert->user_id = User::inRandomOrder()->first()->id;
            $advert->save();
        });
    }
}
