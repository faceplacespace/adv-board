<?php

namespace Tests\Feature\Admin;

use App\Advert;
use App\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdvertControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test index.
     *
     * @return void
     */
    public function testIndex()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.adverts.index');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.adverts.index');
        $response->assertViewHas('adverts');
    }

    /**
     * A basic feature test create.
     *
     * @return void
     */
    public function testCreate()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.adverts.create');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.adverts.create');
        $response->assertViewHas('categories');
    }

    /**
     * A basic feature test store.
     *
     * @return void
     */
    public function testStore()
    {
        $authUser = factory(User::class)->create();
        $url = route('admin.adverts.store');

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $categories = factory(Category::class, rand(1, 3))->create();
        $advert = factory(Advert::class)->make(['user_id' => $authUser->id]);

        $response = $this->actingAs($authUser)
            ->post($url,
                array_merge(
                    $advert->toArray(),
                    ['categories' => $categories->pluck('id')->toArray()]
                )
            );

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.adverts.index'));
        $this->assertDatabaseHas('adverts', $advert->toArray());
    }

    /**
     * A basic feature test edit.
     *
     * @return void
     */
    public function testEdit()
    {
        $authUser = factory(User::class)->create();
        $advert = factory(Advert::class)->create();
        $url = route('admin.adverts.edit', $advert);

        $response = $this->get($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->get($url);
        $response->assertOk();
        $response->assertViewIs('admin.adverts.edit');
        $response->assertViewHasAll([
            'advert' => $advert,
            'categories'
        ]);
    }

    /**
     * A basic feature test update.
     *
     * @return void
     */
    public function testUpdate()
    {
        $authUser = factory(User::class)->create();
        $categories = factory(Category::class, rand(1, 3))->create();
        $advert = factory(Advert::class)->create(['user_id' => $authUser->id]);
        $advert->categories()->sync($categories->pluck('id'));

        $url = route('admin.adverts.update', $advert);

        $response = $this->put($url);
        $response->assertRedirect(route('login'));

        $advertModified = factory(Advert::class)->make(['user_id' => $authUser->id]);
        $categories = factory(Category::class, rand(1, 3))->create();

        $response = $this->actingAs($authUser)
            ->put($url,
                array_merge(
                    $advertModified->toArray(),
                    ['categories' => $categories->pluck('id')->toArray()]
                )
            );

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('admin.adverts.index'));
        $this->assertDatabaseHas('adverts', $advertModified->toArray());
        $this->assertDatabaseMissing('adverts', $advert->toArray());
    }

    /**
     * A basic feature test destroy.
     *
     * @return void
     */
    public function testDestroy()
    {
        $authUser = factory(User::class)->create();
        $categories = factory(Category::class, rand(1, 3))->create();
        $advert = factory(Advert::class)->create();
        $advert->categories()->sync($categories->pluck('id'));
        $url = route('admin.adverts.destroy', $advert);

        $response = $this->delete($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->delete($url);

        $response->assertRedirect(route('admin.adverts.index'));
        $this->assertSoftDeleted('adverts', $advert->toArray());
    }

    /**
     * A basic feature test restore.
     *
     * @return void
     */
    public function testRestore()
    {
        $authUser = factory(User::class)->create();
        $categories = factory(Category::class, rand(1, 3))->create();
        $advert = factory(Advert::class)->create([
            'deleted_at' => now()->toDateTimeString()
        ]);
        $advert->categories()->sync($categories->pluck('id'));
        $url = route('admin.adverts.restore', $advert);

        $response = $this->patch($url);
        $response->assertRedirect(route('login'));

        $response = $this->actingAs($authUser)
            ->patch($url);

        $response->assertRedirect(route('admin.adverts.index', ['only_trashed' => 1]));
        $this->assertDatabaseHas(
            'adverts',
            array_merge($advert->toArray(), ['deleted_at' => null])
        );
    }
}
